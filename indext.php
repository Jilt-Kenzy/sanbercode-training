<?php

 require_once("./class/animal.php");
require_once("./class/ape.php");
require_once("./class/frog.php");

$Nanimals = new Animals("Shaun");

echo "Nama hewanya adalah: ".$Nanimals->name."<br>";
echo "Jumlah kakinya: ". $Nanimals->legs. "<br>";
echo "Hewan berdarah Dingin? ". $Nanimals->cold_blooded. "<br><br>";

$sungokong = new Ape("Kera Sakti");

echo "Nama mahluknya adalah: $sungokong->name <br>";
echo "Jumlah kakinya adalah: $sungokong->legs <br>";
echo "Apakah berdarah dingin?: $sungokong->cold_blooded <br>";
echo $sungokong->yell(). "<br><br>";

$kodok = new Frog("Buduk");

echo "Nama mahluknya adalah: $kodok->name <br>";
echo "Jumlah kakinya adalah: $kodok->legs <br>";
echo "Apakah berdarah dingin?: $kodok->cold_blooded <br>";
echo $kodok->jump();

