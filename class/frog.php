<?php

require_once("animal.php");

class Frog extends Animals{
 public $legs = "4";
 public $cold_blooded = "FALSE";

 public function jump(){
  echo "Hop Hop";
 }
}
